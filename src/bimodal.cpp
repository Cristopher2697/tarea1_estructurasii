#include <math.h>
#include "bimodal.hpp"
#define PI 3.1415
#define STRONG_TAKEN 0
#define WEAK_TAKEN 1
#define WEAK_NOT_TAKEN 2
#define STRONG_NOT_TAKEN 3


/**
 * @brief Funcion constructor 
 * @param No recibe parametros
 * @return No devuelve valores
 */ 
    
Bimodal::Bimodal(){}

/**
 * @brief Funcion constructor
 * @param s tamano de la tabla de 2bc
 * @return No devuelve valores
 */ 

Bimodal::Bimodal(int s){
    this->s= s;
    vector<int> BHT_;
    this->BHT= BHT_;
    for (int i = 1; i <= pow(2,this->s); i++) 
      this->BHT.push_back(3); 
    this->GR=0;

}

/*Sets y gets necesarios*/

void Bimodal::setOutcome(int Outcome){
  this->Outcome= Outcome;
}
void Bimodal::setGR(unsigned char GR){
  this->GR= GR;
}
void Bimodal::setbits(int bits){
  this->s= bits;
}
void Bimodal::setBHT(vector<int> BHT){
  this->BHT= BHT;
}

int Bimodal::getOutcome(){
  return Outcome;
}
unsigned char Bimodal::getGR(){
  return GR;
}
int Bimodal::getbits(){
  return s;
}
vector<int> Bimodal::getBHT(){
  return BHT;
}
bool Bimodal::getPrediction(){
  return this->prediction;
}

string Bimodal::getPrediction_correct(){
  return this->prediction_correct;
}
string Bimodal::getPrediction_string(){
  return this->prediction_string
  ;
}


/**
 * @brief Funcion que se encarga de realizar una prediccion asi como de actualizar el contador que corresponda
 * @param Outcome Representa si se toma o no el salto realmente
 * @param PC program counter
 * @return Devuelve una prediccion
 */ 

bool Bimodal::predictor(bool Outcome, unsigned long int PC){
  	this->Outcome=Outcome;

	//Se truncan los bits necesarios del PC
	this->GR=(unsigned char)((int)PC & ((int)(pow(2,this->s)-1)));
	
	//Se indexa la BHT se hace una prediccion y actualiza el contador correspondiente
	switch( this->BHT[this->GR] )
	{
		case STRONG_TAKEN:
      this->prediction=true;

			if(Outcome==1){
				this->BHT[this->GR]=0;
			}
			else
			{
				this->BHT[this->GR]=1;
			}

			break;
		case WEAK_TAKEN:
      this->prediction=true;

			if(Outcome==1){
				this->BHT[this->GR]=0;
			}
			else
			{
				this->BHT[this->GR]=2;
			}
			break;
		case WEAK_NOT_TAKEN:
      this->prediction=false;

			if(Outcome==1){
				this->BHT[this->GR]=1;
			}
			else
			{
				this->BHT[this->GR]=3;
			}
			break;
		case STRONG_NOT_TAKEN:	
      this->prediction=false;
			if(Outcome==1){
				this->BHT[this->GR]=2;
			}
			else
			{
				this->BHT[this->GR]=3;
			}
			break;
	}

	if(this->prediction==this->Outcome)
		this->prediction_correct="Correcto__";
	else
		this->prediction_correct="Incorrecto";
	



	if(prediction)
		this->prediction_string="T";
	else
		this->prediction_string="N";


	
	return prediction;



}

