#include <math.h>
#include "pshare.hpp"
#define STRONG_TAKEN 0
#define WEAK_TAKEN 1
#define WEAK_NOT_TAKEN 2
#define STRONG_NOT_TAKEN 3


/**
 * @brief Funcion constructor 
 * @param No recibe parametros
 * @return No devuelve valores
 */ 
    
Pshare::Pshare(){}

/**
 * @brief Funcion que define los tamanos de las distintas tablas que utiliza el predictor
 * @param int s tamano de la BHT
 * @param ph tamano del contador global
 * @return No devuelve valores
 */ 

Pshare::Pshare(int s, int ph){
    this->s= s;
	this->ph=ph;
    vector<int> BHT_;
    vector<unsigned char> PHT_;
	this->PHT=PHT_;
    this->BHT= BHT_;
    for (int i = 1; i <= pow(2,this->s); i++) 
      this->BHT.push_back(3); 

	for (int i = 1; i <= pow(2,this->s); i++) 
      this->PHT.push_back(0); 


}

/*Sets y gets necesarios*/

void Pshare::setOutcome(int Outcome){
  this->Outcome= Outcome;
}

void Pshare::setbits(int bits){
  this->s= bits;
}
void Pshare::setBHT(vector<int> BHT){
  this->BHT= BHT;
}

int Pshare::getOutcome(){
  return Outcome;
}

int Pshare::getbits(){
  return s;
}
vector<int> Pshare::getBHT(){
  return BHT;
}
bool Pshare::getPrediction(){
  return this->prediction;
}

string Pshare::getPrediction_correct(){
  return this->prediction_correct;
}
string Pshare::getPrediction_string(){
  return this->prediction_string;
}

/**
 * @brief Atributo encargado de realizar una prediccion asi como de actualizar los contadores necesarios
 * @param 	Outcome representa si el salto se toma o no realimente
 * @param	PC program counter
 * @return Devuelve una prediccion
 */ 

bool Pshare::predictor(bool Outcome, unsigned long int PC){
	this->Outcome=Outcome;
	this->PC=PC;
	//Se prepara una mascara para solo utilizar los bits necesarios
	int maskPC=(int)(pow(2,this->s)-1);
	//Se usan solo los bits necesarios de PC
	unsigned char shortPC=(unsigned char)((int)PC & maskPC);
	//Se define el GR que se obtiene de la tabla de registros globales PHT 
	unsigned char GR=this->PHT[shortPC];
	//Xor entre GR y PC
	unsigned char XorResult=GR ^ shortPC;

	//Se realiza la prediccion y se actualizan los contadores necesarios

	switch( this->BHT[XorResult] )
	{
		case STRONG_TAKEN:
      		this->prediction=true;

			if(Outcome==1){
				this->BHT[XorResult]=0;
			}
			else
			{
				this->BHT[XorResult]=1;
			}

			break;
		case WEAK_TAKEN:
     		 this->prediction=true;

			if(Outcome==1){
				this->BHT[XorResult]=0;
			}
			else
			{
				this->BHT[XorResult]=2;
			}
			break;
		case WEAK_NOT_TAKEN:
      		this->prediction=false;

			if(Outcome==1){
				this->BHT[XorResult]=1;
			}
			else
			{
				this->BHT[XorResult]=3;
			}
			break;
		case STRONG_NOT_TAKEN:	
      		this->prediction=false;
			if(Outcome==1){
				this->BHT[XorResult]=2;
			}
			else
			{
				this->BHT[XorResult]=3;
			}
			break;
	}
	if(this->prediction==this->Outcome)
		this->prediction_correct="Correcto__";
	else
		this->prediction_correct="Incorrecto";

	//Se actualiza el registro global que se esta utilizando de la PHT
	GR=GR<<1;
	if(this->Outcome==1){
		GR++;
	}

	//Se trunca a los bits necesarios
	int mask=pow(2,this->ph)-1;
	GR= GR & mask;
	this->PHT[shortPC]=GR;

	if(prediction)
		this->prediction_string="T";
	else
		this->prediction_string="N";

	return prediction;



}

