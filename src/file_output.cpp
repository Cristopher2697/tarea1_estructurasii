#include "file_output.hpp"

using namespace std;

/*Esta Funcion se encarga de generar el archivo de texto de salida asi como los datos que se imprimen en consola/*/
/*Recibe como parametros: 

bool activate_output, indica si se pidio activar la salida de datos o no
int predictor_type, indica el tipo de predictor a utilizar
bool first_time, este parametro le indica a la funcion se se esta llamando por primera vez, para que imprima las lineas del inicio del archivo de salida
string line, representa la linea que se agregara al archivo de salida segun corresponda
vector<int> flags, este vector de enteros contiene las banderas que necesita el programa




*/
void file_output(bool activate_output,int predictor_type, bool first_time, string line,vector<int> flags)
{
    if(!activate_output)
        return;
    else if(first_time){

      
        string first_time;
        if(predictor_type==0){
            first_time="PREDICTOR BIMODAL\nPC            OUTCOME    PREDICTION    CORRECT/INCORRECT";
        }
        else if(predictor_type==1){
            first_time="PREDICTOR PSHARE\nPC            OUTCOME    PREDICTION    CORRECT/INCORRECT";
        }
        else if(predictor_type==2){
            first_time="PREDICTOR GSHARE\nPC            OUTCOME    PREDICTION    CORRECT/INCORRECT";
        }
        else if(predictor_type==3){
            first_time="PREDICTOR POR TORNEO\nPC            OUTCOME    PREDICTION    CORRECT/INCORRECT";
        }
        string Out_Path="../output/out.txt";
        ofstream out;
        out.open(Out_Path, ofstream::out | ofstream::trunc);
        out << first_time<<endl;;
        out.close();
    }
    else{
        string Out_Path="../output/out.txt";
        ofstream out;
        out.open(Out_Path, ofstream::out | ofstream::app);
        out << line<<endl;
        out.close();
    }
}