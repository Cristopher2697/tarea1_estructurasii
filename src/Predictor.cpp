/* Includes*/
#include "Predictor.hpp"
#include "bimodal.hpp"
#include "gshare.hpp"
#include "pshare.hpp"
#include "MetaPredictor.hpp"
#include "file_output.hpp"
#include <math.h>

using namespace std;
/* Ciertas definiciones utiles*/
#define BIMODAL_ 0
#define GSHARE_ 2
#define PSHARE_ 1
#define META_PREDICTOR_ 3


/* Funcion Predictor es la encargada de llamar a los metodos de la clase correspondiente el predictor seleccionado*/
/*Recibe como parametro las flags o argumentos definidos por el usuario*/

void Predictor(vector<int> flags)
{
  //Se coloca la primera linea del archivo se salida
  file_output(flags[4],flags[1],1, "string line",flags);
/* Declaracion de variables necesarias */
  bool outcome_num;                                                  
  int Number_of_branch=0;                                             
  int Correct_Taken_Predictions=0;                                    
  int Correct__NOT_Taken_Predictions=0;
  int Incorrect_Taken_Predictions=0;
  int Incorrect__NOT_Taken_Predictions=0;
  double Percentage_correct_predictions;
  string PC;
  string Outcome;
  string line="";



  /* El siguiente switch es el encargado de hacer la diferencia entre los distintos predictores*/
  switch (flags[1])
  {
  case BIMODAL_:{
    //Se llama el constructor de la clase
    Bimodal* bimodal1 = new Bimodal(flags[0]);

    //Se entra en un bucle para leer todos los saltos
    while(cin>>PC && cin >> Outcome){
      line="";
      //Si no hay mas saltos para leer se termina 
      if(PC.empty() || Outcome.empty())
        break;
      //Se convierte el outcome leido a un booleano 1 para taken 0 not taken
      else{
        if(Outcome=="T")
          outcome_num=1;
        else if(Outcome=="N")
          outcome_num=0;


        //Se llama el metodo predictor y se construye la string que se enviara al documento de salida
        bimodal1->predictor(outcome_num,strtoul(PC.c_str(), NULL, 0));
        line.append(PC);
        line.append("      ");
        line.append(Outcome);
        line.append("        ");
        line.append(bimodal1->getPrediction_string());
        line.append("        ");
        line.append(bimodal1->getPrediction_correct());
        //Se escriben las primeras 5000 lineas en archivo de salida
        if(Number_of_branch<5000)
          file_output(flags[4],flags[1],0, line,flags);

        //Se actualizan los distintos contadores
        if(outcome_num==1){
          if(bimodal1->getPrediction()==1){
            Correct_Taken_Predictions++;
          }
          else{
            Incorrect_Taken_Predictions++;
          }
        }
        else{
          if(bimodal1->getPrediction()==1){
            Incorrect__NOT_Taken_Predictions++;
          }
          else{
            Correct__NOT_Taken_Predictions++;       
          }
        }
  
      }
      
      Number_of_branch++;
    }
    //Se libera memoria
	  delete bimodal1;
  break;
  }  

  case GSHARE_:{
    //Se llama el constructor de la clase
    Gshare* Gshare1 = new Gshare(flags[0],flags[2]);
    //Se entra en un bucle para leer todos los saltos
    while(cin>>PC && cin >> Outcome){
      line="";
      //Si no hay mas saltos para leer se termina 
      if(PC.empty() || Outcome.empty())
        break;
      //Se convierte el outcome leido a un booleano 1 para taken 0 not taken
      else{
        if(Outcome=="T")
          outcome_num=1;
        else if(Outcome=="N")
          outcome_num=0;

        //Se llama el metodo predictor y se construye la string que se enviara al documento de salida
        Gshare1->predictor(outcome_num,strtoul(PC.c_str(), NULL, 0));
        line.append(PC);
        line.append("      ");
        line.append(Outcome);
        line.append("        ");
        line.append(Gshare1->getPrediction_string());
        line.append("        ");
        line.append(Gshare1->getPrediction_correct());
        if(Number_of_branch<5000)
          file_output(flags[4],flags[1],0, line, flags);

        //Se actualizan los distintos contadores
        if(outcome_num==1){
          if(Gshare1->getPrediction()==1){
            Correct_Taken_Predictions++;
          }
          else{
            Incorrect_Taken_Predictions++;
          }
        }
        else{
          if(Gshare1->getPrediction()==1){
            Incorrect__NOT_Taken_Predictions++;
          }
          else{
            Correct__NOT_Taken_Predictions++;       
          }
        }

      }
      
    Number_of_branch++;
    }

	  delete Gshare1;

  break;
  }


  case PSHARE_:{
    //Se llama al constructor
    Pshare* Pshare1 = new Pshare(flags[0],flags[3]);
    //Se entra en el bucle para leer todos los saltos
    while(cin>>PC && cin >> Outcome){
      line="";
      //Si no hay mas saltos que leer se termina ejecucion
      if(PC.empty() || Outcome.empty())
        break;

      else{
        if(Outcome=="T")
          outcome_num=1;
        else if(Outcome=="N")
          outcome_num=0;
        //Se prepara la linea de salida
        Pshare1->predictor(outcome_num,strtoul(PC.c_str(), NULL, 0));
        line.append(PC);
        line.append("      ");
        line.append(Outcome);
        line.append("        ");
        line.append(Pshare1->getPrediction_string());
        line.append("        ");
        line.append(Pshare1->getPrediction_correct());
        if(Number_of_branch<5000)
          file_output(flags[4],flags[1],0, line, flags);


        //Se actualizan los contadores
        if(outcome_num==1){
          if(Pshare1->getPrediction()==1){
            Correct_Taken_Predictions++;
          }
          else{
            Incorrect_Taken_Predictions++;
          }
        }
        else{
          if(Pshare1->getPrediction()==1){
            Incorrect__NOT_Taken_Predictions++;
          }
          else{
            Correct__NOT_Taken_Predictions++;       
          }
        }

      }
      
    Number_of_branch++;
    }

	  delete Pshare1;
  break;
  }


  case META_PREDICTOR_:{
    //Se llama su constructor
    MetaPredictor* MetaPredictor1 = new MetaPredictor(flags[0],flags[2],flags[3]);
    //Se leen todos los saltos
    while(cin>>PC && cin >> Outcome){
      line="";
      if(PC.empty() || Outcome.empty())
        break;

      else{
        if(Outcome=="T")
          outcome_num=1;
        else if(Outcome=="N")
          outcome_num=0;
        //Se prepara el archivo de salida
        MetaPredictor1->predictor(outcome_num,strtoul(PC.c_str(), NULL, 0));
        line.append(PC);
        line.append("      ");
        line.append(Outcome);
        line.append("        ");
        line.append(MetaPredictor1->getPrediction_string());
        line.append("        ");
        line.append(MetaPredictor1->getPrediction_correct());
        if(Number_of_branch<5000)
          file_output(flags[4],flags[1],0, line, flags);

        //Se actualizan contadores
        if(outcome_num==1){
          if(MetaPredictor1->getPrediction()==1){
            Correct_Taken_Predictions++;
          }
          else{
            Incorrect_Taken_Predictions++;
          }
        }
        else{
          if(MetaPredictor1->getPrediction()==1){
            Incorrect__NOT_Taken_Predictions++;
          }
          else{
            Correct__NOT_Taken_Predictions++;       
          }
        }

      }
      
    Number_of_branch++;
    }


	  delete MetaPredictor1;


  break;
  }
}
  //Se imprime en consola los contadores de ser requerido
    vector<string> predictor_types={"BIMODAL", "PSHARE", "GSHARE", "META_PREDICTOR"};
    Percentage_correct_predictions=100.0*((double)(Correct_Taken_Predictions+Correct__NOT_Taken_Predictions))/((double)Number_of_branch);
    cout<<"--------------------------------------------------------------"<<endl;
    cout<<"Prediction Parameter"<<endl;
    cout<<"--------------------------------------------------------------"<<endl;
    cout<<"Branch Prediction Type:                             "<<predictor_types[flags[1]]<<endl;
    cout<<"BHT size (entries):                                 "<<pow(2,flags[0])<<endl;
    cout<<"Global History register File:                       "<<pow(2,flags[2])<<endl;
    cout<<"Private history register size:                       "<<pow(2,flags[3])<<endl;
    cout<<"--------------------------------------------------------------"<<endl;
    cout<<"Simulation Results:"<<endl;
    cout<<"--------------------------------------------------------------"<<endl;
    cout<<"Number of Branch                                        "<<Number_of_branch<<endl;
    cout<<"Number of correct prediction of taken branches:         "<<Correct_Taken_Predictions<<endl;
    cout<<"Number of incorrect prediction of taken branches:       "<<Incorrect_Taken_Predictions<<endl;
    cout<<"Correct prediction of not taken branches:               "<<Correct__NOT_Taken_Predictions<<endl; 
    cout<<"Incorrect prediction of not taken branches:             "<<Incorrect__NOT_Taken_Predictions<<endl;
    cout<<"Percentage of correct predictions:                      "<<Percentage_correct_predictions<<endl;                                        
    cout<<"--------------------------------------------------------------"<<endl;
  

}