/*
  Proyecto creado por: Cristopher Hernandez, B53346. Contacto cristopher533@gmail.com

  En este programa se implementan los predictores de branches Bimodal, Gshare, Pshare y Metapredictor
  Los predictores reciben un outcome y un pc correspondiente a un branch o salto y se encargan de predecir si los siguientes saltos se 
  tomaran o no segun distintos metodos para cada predictor

  Se implemento una clase para cada predictor con distintos atributos y metodos explicados en sus correpsondientes clases

  Se implementro una funcion llamada file output que se encarga de manejar el archivo de salida si asi se requiere

  Se implemento una funcion llamada flag reader que se encarga de leer las banderas procedentes de los argumentos del usuario

  Se implemento una funcion llamada Handle_exceptions para arrojar advertencias cuando el usuario utiliza argumentos invalidos


*/



#include "flag_reader.hpp"
#include "handle_exceptions.hpp"
#include <chrono>
#include "Predictor.hpp"





using namespace std;


int main(int argc, char const *argv[])
{

  //Funcion para medir el tiempo de ejecucion
  auto start = chrono::steady_clock::now();


  //Se leen los argumentos
  vector<int> flags=flag_reader(argc, argv);
  //Se verifica que sean valores correctos
  handle_exceptions(flags);

  //Se hace un llamado a la funcion predictor 
  Predictor(flags);


  //Se calcula el tiempo de ejecucion
  auto end = chrono::steady_clock::now();
  auto diff = end - start;

  cout << "Tiempo de ejecucion: "<<chrono::duration <double, ratio<1>> (diff).count() << " s" << endl;

}

