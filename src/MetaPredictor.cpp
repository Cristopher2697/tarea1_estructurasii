#include <math.h>
#include "MetaPredictor.hpp"
#define PI 3.1415
#define STRONG_PREFER_GSHARE 0
#define WEAK_PREFER_GSHARE 1
#define WEAK_PREFER_PSHARE 2
#define STRONG_PREFER_PSHARE 3


/**
 * @brief Funcion constructor 
 * @param No recibe parametros
 * @return No devuelve valores
 */ 
    
MetaPredictor::MetaPredictor(){}

/**
 * @brief Funcion constructor
 * @param s representa el tamano de la tabla de 2bc del metapredictor
 * @param gh representa el tamano de los global registers
 * @param ph representa el tamano de la PHT
 * @return No devuelve valores
 */ 

MetaPredictor::MetaPredictor(int s, int gh, int ph){
    this->s= s;
	this->gh= gh;
	this->ph= ph;
    vector<int> MT_;
    this->MT= MT_;
    for (int i = 1; i <= pow(2,this->s); i++) 
      this->MT.push_back(3); 

    Gshare* Gshare1 = new Gshare(s, gh);
    Pshare* Pshare1 = new Pshare(s, ph);
	this->psharePredictor=Pshare1;
	this->gsharePredictor=Gshare1;
	this->GR=0;

}

/*Sets y gets necesarios*/


int MetaPredictor::getOutcome(){
  return Outcome;
}

vector<int> MetaPredictor::getMT(){
  return this->MT;
}
bool MetaPredictor::getPrediction(){
  return this->prediction;
}

string MetaPredictor::getPrediction_correct(){
  return this->prediction_correct;
}
string MetaPredictor::getPrediction_string(){
  return this->prediction_string;
}
string MetaPredictor::getSelectedPredictor(){
  return this->selectedPredictor;
}


/**
 * @brief Funcion que que se encarga de realizar la prediccion utiliza tanto el gshare como pshare y una tabla de 2bc que indica cual usar a la vez que se actualiza como corresponda
 * @param Outcome representa si se toma o no realmente el salto
 * @return Devuelve una prediccion
 */ 

bool MetaPredictor::predictor(bool Outcome, unsigned long int PC){
	//Se hace un llamado a ambos predictores con el PC correspondiente y el outcome
	this->Outcome=Outcome;
	this->gsharePredictor->predictor(Outcome,PC);
	this->psharePredictor->predictor(Outcome,PC);

	//Se toman unicamente los bits necesarios del PC para indexar la tabla del metapredictor
	this->GR=(unsigned char)((int)PC & ((int)(pow(2,this->s)-1)));

	//Se selecciona una de las dos predicciones segun corresponda
	if((this->MT[this->GR]==STRONG_PREFER_GSHARE) || (this->MT[this->GR]==WEAK_PREFER_GSHARE)){
		this->prediction=this->gsharePredictor->getPrediction();
		this->selectedPredictor="G";
	}
	else{
		this->prediction=this->psharePredictor->getPrediction();
		this->selectedPredictor="P";


	}

	


	if(this->prediction==this->Outcome)
		this->prediction_correct="correct";
	else
		this->prediction_correct="incorrect";



	if(prediction)
		this->prediction_string="T";
	else
		this->prediction_string="N";

	//Se actualiza el contador correspondiente segun ambas predicciones

	if(this->gsharePredictor->getPrediction()!=this->psharePredictor->getPrediction()){
		if((this->gsharePredictor->getPrediction()==this->Outcome) && (this->MT[this->GR]!=0)) 
			this->MT[this->GR]--;
		else if((this->psharePredictor->getPrediction()==this->Outcome) && this->MT[this->GR]!=3) 
			this->MT[this->GR]++;

	}

	



	
	return prediction;



}

