#include <math.h>
#include "gshare.hpp"
#define STRONG_TAKEN 0
#define WEAK_TAKEN 1
#define WEAK_NOT_TAKEN 2
#define STRONG_NOT_TAKEN 3


/**
 * @brief Funcion constructor de Gshare
 * @param No recibe parametros
 * @return No devuelve valores
 */ 
    
Gshare::Gshare(){}

/**
 * @brief Funcion constructora que define el tamno de la Branch history table
 * @param int s Cantidad de bits a utilizar para indexar la BHT
 * @param gh Tamano del registro de prediccion global
 * @return No devuelve valores
 */ 

Gshare::Gshare( int s,int gh){
    this->s= s;

    vector<int> BHT_;
    this->BHT= BHT_;
    for (int i = 1; i <= pow(2,this->s); i++) 
        this->BHT.push_back(3); 

    this->n=s;
    this->gh=gh;
    this->GR=0;

}

/* Metodos sets y gets necesarios*/

void Gshare::setOutcome(int Outcome){
  this->Outcome= Outcome;
}


int Gshare::getOutcome(){
  return Outcome;
}
unsigned char Gshare::getGR(){
  return GR;
}


bool Gshare::getPrediction(){
  return this->prediction;
}

string Gshare::getPrediction_correct(){
  return this->prediction_correct;
}
string Gshare::getPrediction_string(){
  return this->prediction_string;
}


/**
 * @brief Este atributo se encarga de hacer la prediccion asi como de actualizar los contadores necesarios
 * @param Outcome Le dice al atributo si el salto se tomo o no realmente
 * @param PC Program counter
 * @return Devuelve la prediccion
 */ 

bool Gshare::predictor(bool Outcome, unsigned long int PC){

    this->Outcome=Outcome;
    this->PC=PC;
	//Se prepara una mascara con la cantidad de bits del contador global
    int mask=pow(2,this->gh)-1;
	//Se realiza una xor entre PC y registro global
    unsigned char pointer= this->GR ^ (unsigned char)this->PC;
	//COn una and se toman los bits necesarios unicamente
    int maskS=pow(2,this->n)-1;
    pointer=pointer & maskS;

	//Se realiza una prediccion y se actualizan contadores

	switch( this->BHT[pointer] )
	{
		case STRONG_TAKEN:
            this->prediction=true;

			if(Outcome==1){
				this->BHT[pointer]=0;
			}
			else
			{
				this->BHT[pointer]=1;
			}

		break;
		case WEAK_TAKEN:
            this->prediction=true;

			if(Outcome==1){
				this->BHT[pointer]=0;
			}
			else
			{
				this->BHT[pointer]=2;
			}
		break;
		case WEAK_NOT_TAKEN:
            this->prediction=false;

			if(Outcome==1){
				this->BHT[pointer]=1;
			}
			else
			{
				this->BHT[pointer]=3;
			}
		break;
		case STRONG_NOT_TAKEN:	
            this->prediction=false;
			if(Outcome==1){
				this->BHT[pointer]=2;
			}
			else
			{
				this->BHT[pointer]=3;
			}
		break;
	}
    
	if(this->prediction==this->Outcome)
		this->prediction_correct="Correcto__";
	else
		this->prediction_correct="Incorrecto";
	

	


	if(prediction)
		this->prediction_string="T";
	else
		this->prediction_string="N";




	//Se actualiza el registro global desplazandolo a la izquierda e insertando el outcome, se usa la mascara con los bits necesarios
    this->GR=this->GR<<1;
    if(this->Outcome==1){
        this->GR++;

    }
    this->GR=this->GR & mask;

    return prediction;

}

