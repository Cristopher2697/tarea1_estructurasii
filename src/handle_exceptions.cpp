#include "handle_exceptions.hpp"


using namespace std;
/*Funcion encargada de verificar la validez de los argumentos, recibe el vector de banderas*/
void handle_exceptions(vector<int> flags)
{
    
    if(flags[1]>3){
        cout<<"La bandera bp representa el tipo de predictor debe estar entre [0 , 3] para bimodal, pshare, gshare y metapredictor respectvamente  "<<endl;
        exit(-1);
    }
    if(flags[4]!=0 && flags[4]!=1){
        cout<<"La bandera -o debe ser 0 o 1 para imprimir o no un archivo de texto con los resultados"<<endl;
        exit(-1);
    }
    

}