/*
Clase del predictor MetaPredictor, utiliza tanto la clase gshare como pshare para comparar ambas predicciones
Y mediante una tabla de 2bc selecciona una de las dos predicciones 
Tambien actualiza el contador correspondiente segun el resultado de las predicciones


*/


#ifndef MetaPredictor_
#define MetaPredictor_

#include <iostream>
#include <vector>
#include "pshare.hpp"
#include "gshare.hpp"
using namespace std;

class MetaPredictor{

public:

    MetaPredictor();                                        //constructor vacio
    MetaPredictor( int s, int gh, int ph);                  //Constructor recibe como parametro s el tamano de la tabla de 2bc, gh el tamano del contador global y ph el tamano de la PHT
    virtual ~MetaPredictor(){};                             //destructor
    bool predictor(bool Outcome, unsigned long int PC  );   //Clase Predictor, se encarga de hacer una prediccion usando gshare y pshare asi como actualizar la tabla del meta predictor

    //Sets y gets de los atributos necesarios
    int getOutcome();
    bool getPrediction();
    string getPrediction_correct();
    string getPrediction_string();
    vector<int> getMT();                            
    string getSelectedPredictor();


private:
    bool Outcome;                                   //Representa si se toma o no realmente el salto
    int s;                                          //Cantidad de bits a usar del PC, tambien define la cantidad de 2bc
    int ph;                                         //Tamano de la PHT
    int gh;                                         //Tamano del contador global
    vector<int> MT;                                 //MetaPredictorTable, contiene los 2bc que seleccionan entre los predictores
    bool prediction;                                //Prediccion 
    string prediction_correct;                      //Dice si la predicion fue o no correcta
    string prediction_string;                       //Prediccion MetaPredictor en formato de string
    Pshare* psharePredictor;                        // Predictor pshare
    Gshare* gsharePredictor;                        //Predictor gshare
    unsigned char GR;                               //Contador global para seleccionar uno de los 2bc del metapredictor
    string selectedPredictor;                       //Indica en forma de una string el predictor utilizado
};


#endif


