/*
Clase del predictor Pshare, utiliza tanto una BHT como una PHT para realizar una predicicon

*/


#ifndef Pshare_
#define Pshare_

#include <iostream>
#include <vector>
using namespace std;

class Pshare{

public:

    Pshare();                                         //constructor vacio
    Pshare( int s, int ph);                           //Constructor recibe como parametro s el tamano de la PHT y BHT, ph el tamano de los contadores globales de la PHT
    virtual ~Pshare(){};                              // destructor
    bool predictor(bool Outcome, unsigned long int PC);                  //Clase Predictor, se encarga de hacer una prediccion asi como de actualizar los contadores necesarios

    //Sets y gets de los atributos necesarios
    void setOutcome(int Outcome);
    void setbits(int bits);
    void setBHT(vector<int> BHT);

    int getOutcome();
    int getbits();
    bool getPrediction();
    string getPrediction_correct();
    string getPrediction_string();
    vector<int> getBHT();


private:
    bool Outcome;                                   //Representa si se toma o no realmente el salto
    int s;                                          //Cantidad de bits del contador global, tambien define la cantidad de 2bc
    int ph;                                         //Tamano de la pattern history table
    unsigned long int PC;                           //Program counter
    vector<int> BHT;                                //Arreglo con los 2bc
    vector<unsigned char> PHT;                      //Pattern history table almacena los contadores globales que indican el 2bc a usar
    bool prediction;                                //Prediccion 
    string prediction_correct;                      //Dice si la predicion fue o no correcta
    string prediction_string;                       //Prediccion Pshare en formato de string


};


#endif


