/*
Clase del predictor bimodal
Este predictor no posee una memoria, es indexado por PC hacia una tabla de 2bc donde hace una prediccion y segun el resultado actualiza el contador

*/


#ifndef BIMODAL
#define BIMODAL

#include <iostream>
#include <vector>
using namespace std;

///@class Clase Circulo
class Bimodal{

public:

    Bimodal();                                       //constructor vacio
    Bimodal( int s);                                 //Constructor recibe como parametro las banderas que definen los diferentes tamanos de las tablas
    virtual ~Bimodal(){};                            // destructor
    bool predictor(bool Outcome, unsigned long int PC);           //Clase Predictor, se encarga de actualizar el 2bc que indexa pc, asi como devolver una prediccion

    //Sets y gets de los atributos necesarios
    void setOutcome(int Outcome);
    void setGR(unsigned char GR);
    void setbits(int bits);
    void setBHT(vector<int> BHT);

    int getOutcome();
    unsigned char getGR();
    int getbits();
    bool getPrediction();
    string getPrediction_correct();
    string getPrediction_string();
    vector<int> getBHT();


private:
    bool Outcome;                                   //Representa si se toma o no realmente el salto
    unsigned char GR;                               //Contador global, al no haber memoria sale de los ultimos n bits del PC
    int s;                                          //Cantidad de bits a usar del PC, tambien define la cantidad de 2bc
    vector<int> BHT;                                //Arreglo con los 2bc
    bool prediction;                                //Prediccion 
    string prediction_correct;                      //Dice si la predicion fue o no correcta
    string prediction_string;                       //Prediccion Bimodal en formato de string


};


#endif


