#ifndef     FO
#define     FO

#include <iostream>
#include <fstream>
#include <vector>

using namespace std;
//Header de la funcion encargada de manejar el archivo de salida
void file_output(bool activate_output, int predictor_type ,bool first_time, string line,vector<int> flags);

#endif