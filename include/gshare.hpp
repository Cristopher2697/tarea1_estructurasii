#ifndef GSHARE
#define GSHARE

#include <iostream>
#include <vector>
using namespace std;

/*
Clase Gshare, este predictor utiliza un contador global y una tabla de 2bc indexada por una xor entre el registro global y el PC

*/
class Gshare{

public:
    Gshare();                           //constructor vacio
    Gshare( int s,int gh);      //Recibe como parametro los tamanos de las tablas
    virtual ~Gshare(){};                // destructor
    bool predictor(bool Outcome, unsigned long int PC);     //Metodo encargado de dar una prediccion y realizar las actualizaciones necesarias

    //Sets y gets necesarios
    void setOutcome(int Outcome);
    int getOutcome();
    unsigned char getGR();
    bool getPrediction();
    string getPrediction_correct();
    string getPrediction_string();
    vector<int> getBHT();


private:

    int s;                          //tamano Arreglo donde se almacenan los contadores
    int gh;                         //Cantidad de bits del registro global
    int n;                          //Cantidad de bits a utilizar del resultado de la XOR entre PC y GC
    bool Outcome;                   //Outcome del salto
    unsigned char GR;               //COntador global
    unsigned long int PC;           //Program counter
    string prediction_correct;      //Dice si fue o no correcta la prediccion
    string prediction_string;       //Prediccion string
    bool prediction;                //Prediccion booleana
    vector<int> BHT;                //Branch history table



};


#endif


