Tarea1_EstructurasII_
    Este Proyecto es Creado por Cristopher Hernandez, B53346, contacto: cristopher533@gmail.com//andres.hernandezarguedas@ucr.ac.cr
   
Consiste en un predictor de branches utilizando 4 metodos o tipos de predictores distintos: Bimodal, Pshare, Gshare y Metapredictor o Predictor por Torneo

El makefile se encuentra en la carpeta /src

Al compilar es posible que se utilice sudo para copiar el binaro creado en la carpeta /usr/bin, permitiendo asi poder correr el ejecutable con solo su nombre como lo sugiere el enunciado 

Para correr el programa puede utilizar "make bimodal", "make gshare", "make pshare" o "make metapredictor" segun lo desee.

    Por defecto se utilizan los siguientes parametros para cada predictor:

        bimodal:             gunzip -c ../data/branch-trace-gcc.trace.gz | branch -s 3 -bp 0 -gh 4 -ph 3 -o 1

        pshare:              gunzip -c ../data/branch-trace-gcc.trace.gz | branch -s 5 -bp 1 -gh 3 -ph 2 -o 1

        gshare:              gunzip -c ../data/branch-trace-gcc.trace.gz | branch -s 4 -bp 2 -gh 3 -ph 3 -o 1
        
        metapredictor:       gunzip -c ../data/branch-trace-gcc.trace.gz | branch -s 3 -bp 3 -gh 4 -ph 3 -o 1




Requisitos
    gunzip
